Description
-----------
This module adds a new type of Webform component: Popup Box.

How to use
----------
- Go to the main field list of a webform.
- Add a Popup Box component to your form.
- Save the component.
- Add or drag other fields into the container.
- If you need a title and/or description for your group you can add markup to the form, or insert the Popup box container into a fieldset.
