(function ($) {
   Drupal.behaviors.webformPopup = {
    attach: function (context, settings) {
      // Hide popup box fieldset container.
      $('.webform-popup-box', context).addClass('hide');

      // Attachs popup dialog box to webform and assign required fields those are
      // under Popup container in webform fieldset.
      function webformFieldsDialog(form_id, success) {
       if ($("#" + form_id + " #webform-dialog", context).length) {
          var form_elements = $("#" + form_id + " #webform-dialog", context).clone();
          $("#" + form_id + " #webform-dialog", context).addClass('hide');
          var formdialog = $(form_elements).dialog({
            autoOpen: false,
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            title: Drupal.settings.webform_popup.popup_box_title,
            buttons: {
              Submit: function () {
                var popup_fields_val = [];
                // Fetch field values.
                $('.ui-dialog #webform-dialog .webform-popup-box input', context).each(function (index) {
                  popup_fields_val[$(this).attr('id')] = $(this).val();
                });
                success(this, popup_fields_val);
                $(this).dialog("close");
              },
              Cancel: function () {
                $(this).dialog("close");
              }
            }
          });
          // Give an ID attribute to the 'Submit' Button.
          $('.ui-dialog .ui-dialog-buttonpane button:contains(Submit)').attr("id", "dialog-confirm_ok-button");
          // Change text of 'Submit' button by its ID given by field settings.
          $('.ui-dialog #dialog-confirm_ok-button').html(Drupal.settings.webform_popup.submit_label);
          return formdialog.dialog("open");
        }
      }

      // Add popup dialog box to webform submit.
      $(".webform-client-form", context).submit(function(event) {
        var form = this;
        var form_id = $(this).attr('id');
        if (typeof $("#"+ form_id + " #webform-dialog", context).html() != 'undefined') {
          event.preventDefault();
          $("#" + form_id + " #webform-dialog", context).removeClass('hide');
          // Display fields under Popup box container in dialog box.
          var webform_popup_box_fields = $("#"+ form_id + " .webform-popup-box", context).clone();
          $(webform_popup_box_fields).removeClass('hide');
          if (!$("#"+ form_id + " #webform-dialog", context).html().length) {
            $("#"+ form_id + " #webform-dialog", context).html(webform_popup_box_fields);
          }

          // Update form field values once the dialog box is submitted with field values.
          webformFieldsDialog(form_id, function (values, field_values) {
             $(values).find('input').each(function(i) {
                var id = $(this).attr('id');
                var input_value = field_values[id];
                $(this).val(input_value);
                var input_cloned = $(this).clone();
                $(input_cloned).attr('type', 'hidden');
                $(input_cloned).appendTo(form);
             });
            form.submit();
          });
        }
      });
    }
  };
})(jQuery);
