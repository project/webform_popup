<?php

/**
 * @file
 * Webform module popup_box component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_popup_box() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'private' => FALSE,
      'submit_text' => t('Submit'),
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_popup_box($component) {
  $form = array();
  $form['display']['submit_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Submit button label'),
    '#default_value' => $component['extra']['submit_text'],
    '#description' => t('By default the submit button on this form will have the label <em>Submit</em>. Enter a new title here to override the default.'),
    '#parents' => array('extra', 'submit_text'),
    '#size' => 60,
  );
  $form['display']['popup_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Popup Box Title'),
    '#default_value' => $component['extra']['popup_title'],
    '#description' => t('Add Popup Box Title.'),
    '#parents' => array('extra', 'popup_title'),
    '#size' => 60,
  );
  // Hide name, it's never shown
  $form['name']['#type'] = 'value';
  // Hack - name is required by webform but we don't expose it to the user. Instead we'll replace it with the value of form_key.
  $form['name']['#value'] = 'box';
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_popup_box($component, $value = NULL, $filter = TRUE) {
  $element = array(
    '#weight' => $component['weight'],
    '#pre_render' => array('webform_popup_box_prerender'),
    '#webform_component' => $component,
    '#prefix' => '<div>',
    '#suffix' => '</div><div id="webform-dialog"></div>',
    '#attached' => array('css' => array(drupal_get_path('module', 'webform_popup') . '/popup_box.css')),
  );
  return $element;
}

/**
 * Pre-render function to set a popup_box ID and classes.
 */
function webform_popup_box_prerender($element) {
  // Add Submit label as drupal settings to use to set the dialog box submit label.
  $dialog_box_submit_label = !empty($element['#webform_component']['extra']['submit_text']) ? $element['#webform_component']['extra']['submit_text'] : t('Submit');
  $popup_box_title = !empty($element['#webform_component']['extra']['popup_title']) ? $element['#webform_component']['extra']['popup_title'] : t('');
  drupal_add_js(array('webform_popup' => array('submit_label' => $dialog_box_submit_label, 'popup_box_title' => $popup_box_title)), 'setting');

  $attributes = empty($element['#attributes']) ? array('class' => array()) : $element['#attributes'];
  $attributes['class'][] = 'webform-popup-box ';
  $attributes['class'][] = 'webform-component--' . str_replace('_', '-', implode('--', array_slice($element['#parents'], 1)));
  $element['#prefix'] = '<div ' . drupal_attributes($attributes) . '>';
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_popup_box($component, $value, $format = 'html') {
  if ($format == 'text') {
    $element = array(
      '#title' => '',
      '#weight' => $component['weight'],
      '#theme_wrappers' => array(),
    );
  }
  else {
    $element = _webform_render_popup_box($component, $value);
  }
  $element['#format'] = $format;
  return $element;
}
